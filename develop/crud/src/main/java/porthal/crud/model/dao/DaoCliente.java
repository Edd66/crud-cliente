/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package porthal.crud.model.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import porthal.crud.model.Cliente;

/**
 * Classe responsável pela persistência dos objetos Cliente
 *
 * @author Eduardo Amaral
 */
public class DaoCliente {

    /**
     * Método que realiza a persistência de um objeto Cliente
     *
     * @param c - Objeto a ser persistido
     * @return - um boolean indicando se o objeto foi salvo ou não
     */
    public static boolean salvar(Cliente c) {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(c);
            tx.commit();
            return true;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }

    }

    /**
     * Método que realiza a busca de todos os objetos do tipo Cliente
     *
     * @return - Um ArrayList com todos os Clientes recuperados no banco
     */
    public static ArrayList<Cliente> consultar() {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        ArrayList<Cliente> c = null;

        try {

            Query q;

            tx = session.beginTransaction();

            q = session.createQuery("FROM Cliente as c");

            c = (ArrayList<Cliente>) q.list();

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();

        } finally {
            session.close();
        }

        return c;

    }

    /**
     * Método que busca um Cliente específico pelo seu id
     *
     * @param id - identificador do Cliente
     * @return - O Cliente especificado
     */
    public static Cliente consultar(int id) {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        Cliente c = null;

        try {

            Query q;

            tx = session.beginTransaction();

            q = session.createQuery("FROM Cliente as c where c.idcliente=:id");

            q.setParameter("id", id);

            List resultados = q.list();

            if (resultados.size() > 0) {
                c = (Cliente) resultados.get(0);
            }

            return c;

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return c;

        } finally {
            session.close();
        }
    }

    /**
     * Método responsável por fazer a consulta com os Joins necessários, para
     * podermos usar o LAZY inicialization.
     *
     * @param id int
     * @return Cliente
     */
    public Cliente consultarWithJoin(int id) {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        Cliente c = null;

        try {

            Query q;

            tx = session.beginTransaction();
            //dois joins, de curso para componente_curso e depois para componente curricular
            q = session.createQuery("FROM Cliente as c LEFT JOIN fetch c.endereco as e LEFT JOIN fetch c.contatos where c.idcliente=:id");

            q.setParameter("id", id);

            List resultados = q.list();

            if (resultados.size() > 0) {
                c = (Cliente) resultados.get(0);
            }

            return c;

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return c;

        } finally {
            session.close();
        }
    }

    /**
     *
     * @param c
     * @return
     */
    public Cliente consultarWithJoin(Cliente c) {
        return this.consultarWithJoin(c.getIdcliente());
    }

    /**
     * Método responsável por realizar a alteração de um objeto Cliente
     *
     * @param c - Variável que contém o objeto modificado
     * @return - Uma variável boolean indicando se o salvamento foi bem sucedido
     */
    public static boolean alterar(Cliente c) {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.merge(c);
            tx.commit();
            return true;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    /**
     * Método responsável por excluir um registro referente a um objeto Cliente
     *
     * @param d - O objeto referente ao registro que deve ser excluido do banco
     * @return - Um boolean indicando se o salvamento foi bem sucedido
     */
    public static boolean excluir(Cliente d) {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(d);
            tx.commit();
            return true;
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    /**
     * Método responsável por mostrar a quantidade de registros na tabela de
     * Cliente
     *
     * @return - O número de registros na tabela
     */
    public static int count() {
        Session session;
        session = ConexaoHibernate.getInstance();
        Transaction tx = null;

        try {

            Criteria crit = session.createCriteria(Cliente.class);
            crit.setProjection(Projections.rowCount());
            Long l = (Long) crit.list().get(0);

            return Integer.valueOf(l.toString());

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return 0;
    }
}

