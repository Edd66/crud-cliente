/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package porthal.crud.model.dao;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import porthal.crud.model.Cliente;
import porthal.crud.model.Endereco;

/**
 *
 * @author eduar
 */
public class DaoClienteTest {

    private Cliente cliente;

    public DaoClienteTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        DaoCliente.excluir(cliente);
    }

    /**
     * Teste do método para salvar positivamente um cliente.
     */
    @Test
    public void testSalvarPositivo() {
        System.out.println("##########################################Testando método salvar##########################################");
        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        boolean expResult = true;
        boolean result = DaoCliente.salvar(cliente);
        assertEquals(expResult, result);
    }

     /**
     * Teste do método para salvar negativamente um cliente.
     */
    @Test
    public void testSalvarSemNome() {
        System.out.println("##########################################Testando método salvar sem o atibuto nome##########################################");
        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        boolean expResult = false;
        boolean result = DaoCliente.salvar(cliente);
        assertEquals(expResult, result);
    }
    
     /**
     * Teste do método para salvar negativamente um cliente.
     */
    @Test
    public void testSalvarcpfIgual() {
        System.out.println("##########################################Testando método salvar com dois cpfs iguais##########################################");
        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);
        
        DaoCliente.salvar(cliente);
        

        //Criar um cliente com o mesmo cpf
        Cliente cliente2 = new Cliente();
        cliente2.setNome("Eduardo Amaral");
        cliente2.setCpfCnpj("123123");
        cliente2.setDataNasc(data);

        //Criar um endereço
        endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente2.setEndereco(endereco);

        boolean expResult = false;
        boolean result = DaoCliente.salvar(cliente2);
        assertEquals(expResult, result);
        
        DaoCliente.excluir(cliente2);
    }
    
    /**
     * Teste do método para consultar uma lista com todos os clientes
     * cadastrados.
     */
    @Test
    public void testConsultar_0args() {
        System.out.println("##########################################Testando método Consultar##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);

        ArrayList<Cliente> array = DaoCliente.consultar();

        int expResult = DaoCliente.count();
        int result = array.size();

        assertEquals(expResult, result);
    }

    /**
     * Test of consultar method, of class DaoCliente.
     */
    @Test
    public void testConsultar_int() {
        System.out.println("##########################################Testando método Consultar##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);

        int id = cliente.getIdcliente();
        Cliente expResult = cliente;
        Cliente result = DaoCliente.consultar(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of consultarWithJoin method, of class DaoCliente.
     */
    @Test
    public void testConsultarWithJoin_int() {
        System.out.println("##########################################Testando método ConsultarWithJoin_int##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);

        DaoCliente instance = new DaoCliente();
        int id = cliente.getIdcliente();
        Cliente c = instance.consultarWithJoin(id);

        Endereco expResult = endereco;
        Endereco result = c.getEndereco();
        assertEquals(expResult, result);
    }

    /**
     * Test of consultarWithJoin method, of class DaoCliente.
     */
    @Test
    public void testConsultarWithJoin_Cliente() {
        System.out.println("##########################################Testando método ConsultarWithJoin_cliente##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);

        DaoCliente instance = new DaoCliente();
        Cliente expResult = cliente;
        Cliente result = instance.consultarWithJoin(cliente);
        assertEquals(expResult, result);
    }

    /**
     * Test of alterar method, of class DaoCliente.
     */
    @Test
    public void testAlterar() {
        System.out.println("##########################################Testando método alterar##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);
        
        cliente.setNome("Eduardo Amaral Alterado");
        
        DaoCliente.alterar(cliente);
        
        String expResult = "Eduardo Amaral Alterado";
        String result = DaoCliente.consultar(cliente.getIdcliente()).getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of excluir method, of class DaoCliente.
     */
    @Test
    public void testExcluir() {
        System.out.println("##########################################Testando método excluir##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);
        
        boolean expResult = true;
        boolean result = DaoCliente.excluir(cliente);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of count method, of class DaoCliente.
     */
    @Test
    public void testCount() {
       System.out.println("##########################################Testando método count##########################################");

        Date data = new Date("1995/09/28");

        //Criar um cliente
        cliente = new Cliente();
        cliente.setNome("Eduardo Amaral");
        cliente.setCpfCnpj("123123");
        cliente.setDataNasc(data);

        //Criar um endereço
        Endereco endereco = new Endereco();
        endereco.setBairro("Centro");
        endereco.setLogradouro("Barão do Cerro Largo");
        endereco.setNumero(459);
        endereco.setCep("97542-080");

        //Settar endereço
        cliente.setEndereco(endereco);

        DaoCliente.salvar(cliente);
        
        int expResult = DaoCliente.consultar().size();
        int result = DaoCliente.count();
        assertEquals(expResult, result);
    }

}
